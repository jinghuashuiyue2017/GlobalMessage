cc.Class({
    extends: cc.Component,

    properties: {

    },

    // onLoad () {},

    start () {
        GlobalMsg.AddListener(111,this.Callback1,this);
        GlobalMsg.AddListener("222",this.Callback2,this);
        GlobalMsg.Start();
    },

    // update (dt) {
    // },

    OnButton1()
    {
        cc.log("OnButton1");
        GlobalMsg.PostMsg(111,0,"111111111111");
    },
    OnButton2()
    {
        cc.log("OnButton2");
        GlobalMsg.PostMsg("222",300,"22222222222");
    },
    Callback1(msg)
    {
        cc.log(msg);
    },
    Callback2(msg)
    {
        cc.log(msg);
        GlobalMsg.DeleteListenerByTarget(111,this);
    }
});
